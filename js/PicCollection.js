var PictureModel = require('./PictureModel.js');

module.exports = Backbone.Collection.extend({

	model : PictureModel,
	url : 'https://api.flickr.com/services/feeds/photos_public.gne',
	tags : 'potato',

	sync : function(method, collection, options) {

		// Override the Backbone.sync method to work with flickr's jsonp service
		// Add the tag(s) and request parameters to return data as jsonP from Flickr
		// Change the dataType property to jsonp and specify the named callback

		options.data = {
			tags: this.tags,
			tagmode:'all',
			format:'json'
		}
    
	    options.dataType = 'jsonp';
	    options.jsonpCallback = 'jsonFlickrFeed';
	    return Backbone.sync(method, collection, options);
  	},

  	parse: function(data){

  		// Overwrite the default parse method to return the array of images as the collection
  		return data.items;
  	}

});

