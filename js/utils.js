// Convert a string to a hash. Useful for generating unique ids for models based on string properties
var stringToHash = function(str) {
    var hash = null;
    str.split('').forEach(function(letter) {
        hash = letter.charCodeAt() + ((hash << 5) - hash);
    });
    return hash;
};


module.exports = {
	stringToHash : stringToHash
}