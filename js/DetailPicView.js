var Mn = require('backbone.marionette');
var detailTpl = require('templates/detail.handlebars');
var $ = require('jquery');
var router = require('Router');
var flickrChannel = Backbone.Wreqr.radio.channel('flickr');

module.exports = Mn.ItemView.extend({

	template : detailTpl,
	tagName : 'div',
	className: 'photo-detail',

	templateHelpers: function(){	
		// Image is tagged with single words separated by a space.
		// Split these up into a list so we can select each individually 
		return {
			tagList : (this.model.get('tags') || '').split(' ')
		}
	},

	ui: {
		tagLink: '.tag'
	},

	events: {
		'click @ui.tagLink': 'navToTagList'
	},

	navToTagList: function(e){

		// Prevent immediate navigation to the url
		// The previous list view will be cached. 
		// If there is a collection and the tag has changed, empty the collection to clear the old list from the view
		// Then trigger the navigation manually through the router and allow the app to refresh itself
		// e.preventDefault();
		// if(this.model.collection && this.model.collection.tags !== $(e.currentTarget).text()){
		// 	this.model.collection.reset([], {silent:true});
		// }
		// return flickrChannel.vent.trigger('navigate', e.currentTarget.href);
	}

});