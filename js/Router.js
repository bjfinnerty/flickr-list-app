var Mn = require('backbone.marionette');
var app = require('./App.js');

module.exports = Mn.AppRouter.extend({

	appRoutes: {
		'tag/:tag': 'list',
		'tag/:tag/id/:id': 'detail',
		'': 'redirectToDefault'
	},

	controller : {
		list: function(tag){
			// show a list of images using a tag provided by the url
			app.rootView.showList(tag);
		},

		detail: function(tag, photoId){
			// Show a more detailed view of the image
			app.rootView.showDetail(tag, photoId);
		},

		redirectToDefault: function(){
			// If there is no url provided start here
			app.rootView.router.navigate('/tag/potato', {trigger:true});
		}
	}

});