var moment = require('moment');

module.exports = function(date, format) {

    if (date && format) {
        return moment(date).format(format);
    }else{
    	throw 'momentFormat helper requires 2 arguments'
    }
};