var Mn = require('backbone.marionette');
var RootView = require('./RootView.js');

var app = new Mn.Application({
    container : 'body',
    rootView :  new RootView()
});

module.exports = app;