var stringToHash = require('./utils.js').stringToHash;

module.exports = Backbone.Model.extend({

	initialize: function(options){

		// Generate a unique id for this image model and set it as a property on the model
		// Extract the authors username for display
		this.setId();
		this.setAuthorName();
	},

	setId:function(){

		// Hash the url for this image and cast it to a string
		// This will give us a true unique id for the model

		if(this.get('link')){
			var id = stringToHash(this.get('link')).toString();
			this.set('id', id);
		}
	},

	setAuthorName: function(){

		// The author field is messy Eg. nobody@flickr.com (ranjit)
		// Extract the real username from this property for display purposes

		// TODO: Fix this up. Ugly
		if(!this.get('author_name')){
			var regExp = /\(([^)]+)\)/;
			var matches = regExp.exec(this.get('author'));
			if(matches && matches.length){
				this.set('author_name', matches[1]);
			}
		}
	}

	
});