var urlParse = require('url-parse');
var Mn = require('backbone.marionette');
var PicListView = require('./PicListView.js');
var PicCollection = require('./PicCollection.js');
var DetailPicView = require('./DetailPicView.js');
var LoadingPanel = require('./LoadingPanel.js');


var flickrChannel = Backbone.Wreqr.radio.channel('flickr');

module.exports = Mn.LayoutView.extend({
	
	el: '#app',

	regions : {
		"pictureList": "#picture-list"
	},

	initialize: function() {
		// Initialize the collection here. Not too sure about the validity of this as regards modularity
		// The main reason was that so we could just pluck the model from the collection using the url  
		// But after some thought it would probably be better to keep this in the collection view
		this.collection = new PicCollection();
		this.delegateEvents();
		// Listen to the flickr event chanel navigate event that will come from the detail view
		this.listenTo(flickrChannel.vent, 'navigate', this.navigateTo);
	},

	navigateTo:function(url){

		// Parse the url and direct the app to the hash 
		var parsedUrl = new urlParse(url);
		return this.router.navigate(parsedUrl.hash, {trigger:true});
	},

	showList:function(tag){

		// Show a new list based on the tag provided
		// If this function has previously been called and the tag hasn't changed, the view will be cached so we can just show that by doing nothing
		if(this.collection.tag !== tag){
			this.getRegion('pictureList').show(new PicListView({
				collection : this.collection,
				tag:tag 
			}));
		}
	},

	showDetail:function(tag, photoId){

		
		var self = this;

		function getModelAndShow(id){
			// pluck the model from the collection using our generated hash id
			// set the tag as a property on the model
			// Render the detailed view
			var model = self.collection.get(id) || self.collection.findWhere({id:id});
			model.set('tag', tag);
			self.getRegion('pictureList').empty().show(new DetailPicView({
				model : model
			}));
		}

		if(this.collection.length){
			// pluck the model from the collection and render it using the detailed view
			return getModelAndShow(photoId)
		}else{
			// If the user refreshes the page, we can reset the collection from the server using the tag in the url
			this.collection.tags = tag;
			this.collection.fetch().then(function(){
				return getModelAndShow(photoId)
			});
		}
	}

});
