var Mn = require('backbone.marionette');
var picListItemTpl = require('./templates/listItem.handlebars');


var ListChildView = Mn.ItemView.extend({

	// Childview of All Pictures view
	// Renders each row in the list using the template and the PictureModel
    tagName: 'li',
    template : picListItemTpl

});


module.exports = Mn.CollectionView.extend({

	tagName: 'ul',
	className : 'tag-list',
	childView : ListChildView,

	// Set the collections tag as a property on the model.
	// We will use that for the back button in the detail view
	childViewOptions: function(model, index){
		model.set('tag', this.options.tag)
	},

	// Fetch the collection when the view is shown
	// View will automatically rerender on the collection reset
	onShow: function(){
		console.log('show loader');
		this.collection.fetch();
	},

	// Set the current tag as a property on the collection
	// This tag name comes from the url
	initialize: function(options){
		this.options = options;
		this.collection.tags = options.tag
	}
	
});



