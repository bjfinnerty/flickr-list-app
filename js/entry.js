var Router = require('./Router.js')

require('./../styles/app.less');

var app = require('App.js');

// When the app starts create a router and reference it in the rootView
// Start backbone.history to monitor hash change events
app.addInitializer(function(options){
	this.rootView.router = new Router();
	Backbone.history.start();
});

app.start();
