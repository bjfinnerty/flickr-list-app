var utils = require('../js/utils.js');

describe("Utility function string to hash", function() {
	
	it("should return a hash when given a string", function() {

		var hashOfHello = 99162322;
		var hash = utils.stringToHash('hello');

		expect(hash.constructor).toEqual(Number);
		expect(hash).toEqual(99162322);

	});

});