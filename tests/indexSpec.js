
describe("Test tests", function() {
	
	it("should run a test", function() {
		expect(1).toEqual(1)
	});

	it("should require a file", function() {
		var module = require("./fixtures/jsModule.js");
		expect(module.testFunc instanceof Function).toEqual(true);
		
	});

});