var app = require('../js/App.js');
var Mn = require('backbone.marionette');

describe("App module", function() {
	
	it("should return an instance of a Marionette Application", function() {

		expect(app instanceof Mn.Application).toEqual(true);
		expect(app.rootView instanceof Mn.LayoutView).toEqual(true);
		expect(app.container).toEqual('body');

	});

});