module.exports = function(config) {
    config.set({
        // ... normal karma configuration

        // list of files / patterns to load in the browser
        files: [
          'tests/*Spec.js'
        ],

        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        preprocessors: {
            // add webpack as preprocessor
            'tests/*Spec.js': ['webpack']//,
            //'test/**/*_test.js': ['webpack']
        },

        webpack: {
            // karma watches the test entry points
            // (you don't need to specify the entry option)
            // webpack watches dependencies

            // webpack configuration
        },

        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i. e.
            noInfo: true
        },

        plugins: [
            'karma-webpack',
            'karma-jasmine',
            'karma-phantomjs-launcher'
        ],

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        singleRun: true
    });
};