var path = require('path');

module.exports = {
    entry: "./js/entry.js",
    output: {
        publicPath : '/dist',
        path: __dirname + '/dist',
        filename: "bundle.js"
    },

    resolve: {

        root: __dirname,
        modulesDirectories: ['js', 'node_modules'],

  },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.less$/, loader: "style!css!less" },
            { test: /\.json$/, exclude: /node_modules/, loader: 'json' },
            { test: /\.handlebars$/, loader: "handlebars-loader", query: { helperDirs: [path.resolve('js/templates/helpers')] }}
        ]
    }
};